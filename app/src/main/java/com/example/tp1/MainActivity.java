package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    CountryList countryList;

    String[] nomPays;


    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nomPays = countryList.getNameArray();

        listView = (ListView) findViewById(R.id.list_Id);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, nomPays);
        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {

                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {

                        Intent myIntent = new Intent(view.getContext(), CountryActivity.class);
                        String countryname =(String) (listView.getItemAtPosition(position));
                        myIntent.putExtra("country", countryname);
                        startActivityForResult(myIntent, 0);

                    }

        });











    }


}
