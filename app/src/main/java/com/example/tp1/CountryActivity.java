package com.example.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class CountryActivity extends AppCompatActivity implements View.OnClickListener {



    EditText capitale = null;
    EditText langue = null;
    EditText monnaie = null;
    EditText population = null;
    EditText sup = null;



    CountryList countryL;
    Country c;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        capitale =  (EditText)findViewById(R.id.capId);
        langue = (EditText)findViewById(R.id.langue);
        monnaie = (EditText)findViewById(R.id.monnaie);
        population = (EditText)findViewById(R.id.population);
        sup = (EditText)findViewById(R.id.superficie);


        //EditText countryname = null;
        image = (ImageView) findViewById(R.id.imageView);



        Bundle e = getIntent().getExtras();

        c = countryL.getCountry(e.getString("country"));

        int id = getResources().getIdentifier("com.example.tp1:drawable/" +c.getmImgFile() , null, null);
        image.setImageResource(id);

        //countryname.setText(extras.getString("country"));
        capitale.setText(c.getmCapital());
        langue.setText(c.getmLanguage());
        monnaie.setText(c.getmCurrency());
        population.setText(String.valueOf(c.getmPopulation()));
        sup.setText(String.valueOf(c.getmArea()));

        Button button = (Button) findViewById(R.id.IDButton);
        button.setOnClickListener((View.OnClickListener)this);




    }

    public void onClick(View view){
        if (view.getId()==R.id.IDButton){ // C'est notre bouton ? oui, alors affichage d'un message
            Toast.makeText(this,"SAUVEGARDER", Toast.LENGTH_SHORT).show();

            //sauvegarderLesInformation
            String chaine = capitale.getText().toString();
            String chaine1 = langue.getText().toString();
            String chaine2 = monnaie.getText().toString();
            String chaine3 = population.getText().toString();
            int valeur1 =Integer.parseInt(chaine3);
            String chaine4 = sup.getText().toString();
            int valeur2 =Integer.parseInt(chaine4);

            if (c.getmCapital().compareTo(chaine)!= 0) c.setmCapital(chaine);
            if (c.getmLanguage().compareTo(chaine1)!= 0) c.setmLanguage(chaine1);
            if (c.getmCurrency().compareTo(chaine2)!= 0) c.setmCurrency(chaine2);
            if (c.getmPopulation() != valeur1) c.setmPopulation(valeur1);
            if (c.getmArea() != valeur2) c.setmArea(valeur2);
        }
    }




}
